from rest_framework import routers
from .views import ServiceViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'service', ServiceViewSet)

urlpatterns = router.urls