# from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Service
from .serializer import ServiceSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(provider=self.request.user)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)