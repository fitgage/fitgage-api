from .models import Service
from rest_framework import serializers
from drf_extra_fields.geo_fields import PointField

# Serializers define the API representation.
class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    address = serializers.JSONField(binary=False)
    location_coords = PointField(required=False)

    class Meta:
        model = Service
        fields = ['id' ,'title' ,'description' ,'start_date' ,'end_date' ,'start_time' ,'end_time' ,'address' ,'location_coords' ,'price' ,'observations' ,'provider_id' ,'created_at']