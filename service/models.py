import uuid
from django.db import models
from django.utils import timezone
from django.contrib.gis.db import models
from django.contrib.gis.db.models import PointField
from django.contrib.postgres.fields import JSONField

class Service(models.Model):
       
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField('Title', max_length=100, blank=False, null=False)
    description = models.TextField('Description', blank=False, null=False)
    start_date = models.DateField('Start date', blank=True, null=True)
    end_date = models.DateField('End date', blank=True, null=True)
    start_time = models.TimeField('Start time', auto_now=False,  blank=True, null=True)
    end_time = models.TimeField('End time', auto_now=False, blank=True, null=True)
    address = JSONField('Address', blank=False, null=False)
    location_coords = models.PointField('Location Coordinates', srid=4326, blank=True, null=True)
    price = models.DecimalField('Price', max_digits=10, decimal_places=2)
    observations = models.TextField('Observations', blank=True, null=True)
    provider = models.ForeignKey('auth.User', on_delete = models.CASCADE)
    created_at = models.DateTimeField('Created at', default=timezone.now)

    def __str__(self):
        return "%s  %s" % (self.business_name)
    
    class Meta:
        db_table = "service"
