from django.contrib.auth.models import User
from .models import Profile
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from drf_extra_fields.geo_fields import PointField

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    access = serializers.SerializerMethodField()
    refresh = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password', 'access', 'refresh']

    def get_access(self, user):
        refresh = RefreshToken.for_user(user)
        return str(refresh.access_token)

    def get_refresh(self, user):
        refresh = RefreshToken.for_user(user)
        return str(refresh)

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user

class UserMinSerializer(UserSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'access', 'refresh']

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    location = serializers.JSONField(binary=False)
    location_coords = PointField(required=False)

    class Meta:
        model = Profile
        fields = ['name', 'lastname', 'photo', 'birth_date', 'phone', 'location', 'location_coords']