from rest_framework import routers
from .views import UserViewSet, ProfileViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'user/profile', ProfileViewSet)
router.register(r'user', UserViewSet)

urlpatterns = router.urls