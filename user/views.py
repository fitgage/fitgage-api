# from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Profile
from .serializer import UserSerializer, ProfileSerializer, UserMinSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    
    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        user = queryset.get(pk=self.request.user.id)
        serializer = UserMinSerializer(user, many=False)

        return Response(serializer.data)

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]

        return [permission() for permission in permission_classes]

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        user = queryset.get(pk=self.request.user.id)
        serializer = ProfileSerializer(user, many=False)
        
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(pk=self.request.user)
        self.check_object_permissions(self.request, obj)
        return obj