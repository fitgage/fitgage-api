from django.db import models
from django.utils import timezone
from django.contrib.gis.db import models
from django.contrib.gis.db.models import PointField
from django.contrib.postgres.fields import JSONField

class Profile(models.Model):

    user = models.OneToOneField('auth.User', on_delete = models.CASCADE,primary_key = True,)
    name = models.CharField('Name', max_length=100, blank=False, null=False)
    lastname = models.CharField('Lastname', max_length=100, blank=False, null=False)
    photo = models.CharField('Photo', max_length=100, blank=True, null=True)
    birth_date = models.DateField('Birth date', blank=False, null=False)
    phone = models.CharField('Phone', max_length=15, blank=True, null=True)
    location = JSONField('Location', blank=True, null=True, default=list)
    location_coords = models.PointField('Location Coordinates', srid=4326, blank=True, null=True)
    created_at = models.DateTimeField('Created at', default=timezone.now)

    def __str__(self):
        return "%s  %s" % (self.name, self.lastname)