from .models import Provider
from rest_framework import serializers

# Serializers define the API representation.
class ProviderSerializer(serializers.HyperlinkedModelSerializer):
    business_address = serializers.JSONField(binary=False)

    class Meta:
        model = Provider
        fields = ['business_name', 'business_number', 'business_address', 'identity_number', 'terms']