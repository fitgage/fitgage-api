# from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Provider, MPMarketplaceCredential
from .serializer import ProviderSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render, get_object_or_404
import requests

class ProviderViewSet(viewsets.ModelViewSet):
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(pk=self.request.user)
        self.check_object_permissions(self.request, obj)

        return obj

class MPMarketplaceCredentialViewSet(viewsets.ModelViewSet):
    queryset = MPMarketplaceCredential.objects.all()
    #serializer_class = MPMarketplaceCredentialSerializer
    permission_classes = (IsAuthenticated,)
    
    def __init__(self, arg):
        super(MPMarketplaceCredential, self).__init__()
        self.arg = arg

    #  http://www.URL_de_retorno.com?user_id=1&code=AUTHORIZATION_CODE
    """
  -H 'accept: application/json' \
     -H 'content-type: application/x-www-form-urlencoded' \
     'https://api.mercadopago.com/oauth/token' \
     -d 'client_secret=ACCESS_TOKEN' \
     -d 'grant_type=authorization_code' \
     -d 'code=AUTHORIZATION_CODE' \
     -d 'redirect_uri=REDIRECT_URI'
"""    
    def link_account(request, pk, *args, **kwargs):
        """
        user = get_object_or_404(User, pk=pk)
        code = request.GET.get('code')        
        url = 'https://api.mercadopago.com/oauth/token'
        headers = { 
            'content-type': 'application/x-www-form-urlencoded'
            'accept': 'application/json',
            'grant_type': 'text/plain'
            'redirect_uri': 'https://dev.fitgage.us/mp-marketplace-credentials/link-accounts/user/5'
        }

        response = requests.post(url, params={ 'code': 'kdjlkjdsfs'})
        print(response)
"""
        return render(request, 'link_account.html', { })

    def link_finish(request):
        return render(request, 'link_account.html', { })

"""
    def get(self, request, pk=None):
        dog = get_object_or_404(Dog, pk=pk)
        dog.feed()
        return Response ({“msg“: “Dog fed“, status=status.HTTP_200_OK})
"""
"""
class CallViewSet(viewsets.ModelViewSet):
 
    def end_call(self, request, identifier=None):
        call = self.get_object()
        timestamp = request.data.get(‘timestamp‘, None)
        try:
            if not call.has_ended:
            call.end_call ( timestamp = timestamp)
        except ValueError as exc:
            raise serializers.ValidationError(str(exc))
        serializer = self.serializer_class(instance=call)
        return Response(serializer.data, status=status.HTTP_200_OK)
"""