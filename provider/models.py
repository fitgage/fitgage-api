from django.db import models
from django.utils import timezone
from django.contrib.gis.db import models
from django.contrib.postgres.fields import JSONField

class Provider(models.Model):

    user = models.OneToOneField('auth.User', on_delete = models.CASCADE,primary_key = True,)
    business_name = models.CharField('Business Name', max_length=100, blank=False, null=False)
    business_number = models.CharField('Business Num.', max_length=11, blank=False, null=False)
    business_address = JSONField('Business Address', blank=False, null=False)
    # type_identity = models.ManyToOneField('model')
    identity_number = models.CharField('Identification Num.', max_length=100, blank=True, null=True)
    terms = models.BooleanField('Terms & Conditions', blank=False, null=False)
    created_at = models.DateTimeField('Created at', default=timezone.now)

    def __str__(self):
        return "%s  %s" % (self.business_name)

class MPMarketplaceCredential(models.Model):

    user = models.OneToOneField('auth.User', on_delete = models.CASCADE,primary_key = True,)
    authorization_code = models.CharField('Authorization Code', max_length=100, blank=False, null=False)
    public_key = models.CharField('Public Key', max_length=100, blank=False, null=False)
    access_token = models.CharField('Access Token.', max_length=100, blank=False, null=False)
    refresh_token = models.CharField('Refresh Token.', max_length=100, blank=False, null=False)
    created_at = models.DateTimeField('Created at', default=timezone.now)

    def __str__(self):
        return "%s  %s" % (self.business_name)