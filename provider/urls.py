from rest_framework import routers
from .views import ProviderViewSet, MPMarketplaceCredentialViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'provider', ProviderViewSet)

urlpatterns = router.urls